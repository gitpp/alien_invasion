class Settings():
    """存储《外星人入侵》的所有设置的类"""

    def __init__(self):
        """初始化游戏的设置"""
        self.screen_width = 1200
        self.screen_height = 600
        # 背景色
        self.bg_color = (0, 0, 0)

        # 飞船的位置
        self.ship_speed_factor = 1.5

        # 子弹设置: 宽3像素、高15像素的深灰色子弹
        self.bullet_speed_factor = 1
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = 255, 255, 228
        self.bullet_allowed = 3
