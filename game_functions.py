import sys

import pygame
from bullet import Bullet


def check_keydown_events(event, ai_settings, screen, ship, bullets):
    """响应按键"""
    if event.type == pygame.KEYDOWN:
        # 右、左、上、下移动飞船
        if event.key == pygame.K_RIGHT:
            ship.moveing_right = True
        elif event.key == pygame.K_LEFT:
            ship.moveing_left = True
        # elif event.key == pygame.K_UP:
        #     ship.moveing_up = True
        # elif event.key == pygame.K_DOWN:
        #     ship.moveing_down = True
        # 空格键射击
        elif event.key == pygame.K_SPACE:
            # 创建一颗子弹,并将其加入到编组bullets中
            new_bullet = Bullet(ai_settings, screen, ship)
            bullets.add(new_bullet)


def check_keyup_events(event, ship):
    """响应松开"""
    if event.type == pygame.KEYUP:
        if event.key == pygame.K_RIGHT:
            ship.moveing_right = False
        elif event.key == pygame.K_LEFT:
            ship.moveing_left = False
        # elif event.key == pygame.K_UP:
        #     ship.moveing_up = False
        # elif event.key == pygame.K_DOWN:
        #     ship.moveing_down = False


def check_events(ai_settings, screen, ship, bullets):
    # 响应按键和鼠标事件
    for event in pygame.event.get():
        # 退出
        if event.type == pygame.QUIT:
            sys.exit()
        # 按下
        elif event.type == pygame.KEYDOWN:
            check_keydown_events(event, ai_settings, screen, ship, bullets)
        # 松开
        elif event.type == pygame.KEYUP:
            check_keyup_events(event, ship)


def update_screen(ai_settings, screen, ship, bullets):
    """更新屏幕上的图像,并切换到新屏幕"""
    # 每次循环都重绘屏幕
    screen.fill(ai_settings.bg_color)
    # 在飞船和外星人后面重绘所有子弹
    for bullet in bullets.sprites():
        bullet.draw_bullet()
    ship.blitme()

    # 让最近绘制的屏幕可见
    pygame.display.flip()
