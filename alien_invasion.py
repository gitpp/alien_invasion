import pygame
# 导入settings文件里的Settings类
from settings import Settings

from ship import Ship
import game_functions as gf

from pygame.sprite import Group  # 类似于列表，但提供了有助于开发游戏的额外功能


def run_game():
    # 初始化游戏并创建一个屏幕对象
    pygame.init()
    # 创建一个 Settings 实例
    ai_settings = Settings()

    screen = pygame.display.set_mode((ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("外星人大战")

    # 创建一艘飞船
    ship = Ship(ai_settings, screen)
    # 创建一个用于存储子弹的编组
    bullets = Group()

    # 开始游戏的主循环
    while True:
        gf.check_events(ai_settings, screen, ship, bullets)
        # 飞船的位置将在检测到键盘事件后（但在更新屏幕前）更新。
        # 这样，玩家输入时，飞船的位置将更新，从而确保使用更新后的位置将飞船绘制到屏幕上。
        ship.update()
        bullets.update()

        # 删除已消失的子弹
        for bullet in bullets.copy():
            if bullet.rect.bottom <= 0:
                bullets.remove(bullet)
        #print(len(bullets)) # 打印测试,通过
        gf.update_screen(ai_settings, screen, ship, bullets)


run_game()
